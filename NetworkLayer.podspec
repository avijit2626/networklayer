Pod::Spec.new do |spec|

  spec.name         = "NetworkLayer"
  spec.version      = "1.0.1"
  spec.summary      = "A library used for network call"
  spec.description  = <<-DESC
			NetworkLayer library use Alamofire at its core and wrap the network call which returns the specific Api data or the error in the network call in a predefine error format which then can be handle in the project specific level.
                   DESC

  spec.homepage     = "https://www.daffodilsw.com"
  spec.license      = { :type => "MIT", :file => "LICENSE.txt" }
  spec.author       = "daffodil"
  spec.platform     = :ios, "11.4"

  # spec.source       = { :http => 'file:' + __dir__ + "/" }
  spec.source       = { :git => "https://bitbucket.org/avijit2626/networklayer.git", :tag => "#{spec.version}" }

  spec.source_files  = "NetworkLayer/**/*.{swift}"
  spec.swift_version = "5.0"
  spec.dependency 'Alamofire', '5.0.0-beta.7'
  

end
