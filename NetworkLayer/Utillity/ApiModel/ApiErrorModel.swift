import Foundation

///API Error model (Generic)
public struct APIError {
    var code: Int?
    var data: Data?
    var error: NSError?
}
