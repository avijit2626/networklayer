import Foundation

///Types of header available. Customisable
public enum HeaderType {
    case contentType
    case apiKey
    case authorization
}

///Header class. Provide the required key value pair only.
public struct HeaderParameter {
    let type: HeaderType
    let key: String
    let authAdditionalKey: String?
    let value: String

    public init(headerType: HeaderType, headerKey: String, headerAdditionalKey: String?, headerValue: String) {
        self.type = headerType
        self.key = headerKey
        self.authAdditionalKey = headerAdditionalKey
        self.value = headerValue
    }
}
