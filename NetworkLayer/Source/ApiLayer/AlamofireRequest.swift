/**
 This File is used to constructing the API request model.
 */

import Foundation

public struct APIRequest {
    public let url: String!
    public var params: [String: Any]!
    public var httpHeaders: [String: String]
    public init(url: String!, params: [String: Any]!, httpHeaders: [HeaderParameter]) {
        self.url = url
        self.params = params
        self.httpHeaders = [:]

        for eachHeader in httpHeaders {
            switch eachHeader.type {
            case .apiKey:
                //add header
                self.httpHeaders[eachHeader.key] = eachHeader.value
                break
            case .authorization:
                //add header
                if eachHeader.authAdditionalKey != nil {
                    self.httpHeaders[eachHeader.key] = eachHeader.authAdditionalKey! + " " + eachHeader.value
                }
                else {
                    self.httpHeaders[eachHeader.key] = Constants.token + " " + eachHeader.value
                }
                break
            case .contentType:
                //add header
                self.httpHeaders[eachHeader.key] = eachHeader.value
                break
            }
        }
    }
}

public struct MultipartAPIRequest {
    public let url: String!
    public let params: [String: String]
    public var httpHeaders: [String: String]
    public let data: Data?
    public let imgName: String!
    public let dataKeyName: String!

    public init(url: String!, params: [String: String]!, httpHeaders: [HeaderParameter], data: Data?, imgName: String, dataKeyName: String) {
        self.url = url
        self.params = params
        self.httpHeaders = [:]
        self.data = data
        self.imgName = imgName
        self.dataKeyName = dataKeyName

        for eachHeader in httpHeaders {
            switch eachHeader.type {

            case .contentType:
                self.httpHeaders[eachHeader.key] = eachHeader.value
            case .apiKey:
                self.httpHeaders[eachHeader.key] = eachHeader.value
            case .authorization:
                self.httpHeaders[eachHeader.key] = eachHeader.authAdditionalKey! + " " + eachHeader.value
            }
        }
    }

}


