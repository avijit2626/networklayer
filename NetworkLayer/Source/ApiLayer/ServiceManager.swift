/**
 This file is used to perform different network opertion using Alamofire library.
 */

import UIKit
import Alamofire

public class ServiceManager: NSObject {

    // MARK: - Local property
    public static let shared = ServiceManager()

    // MARK: - Helper methods
    private func getJsonStringFor(dictionary: Any) -> String {
        let dataString = String(data: dictionary as? Data ?? Data(), encoding: String.Encoding.utf8)
        return dataString ?? ""
    }

    /**
     This method return NSError object in case if internet connection is not available
     - returns: NSError Object
     */
    private func getNetworkError() -> NSError {
        return NSError(domain: "network_error", code: -1009, userInfo: nil)
    }

    /**
     Method is used to call Alamofire network request.
     - parameter url: URLConvertible type string
     - parameter method: use to set HTTPMethod type
     - parameter parameters: Key-value pair object, used to set request body
     - parameter headers: use to set HTTPHeaders of the request
     - parameter returningClass: T(generic) type parameter, used to decode api response to compatible response model
     - parameter responseDelegate: ApiResponseReceiver type object, used for success/ failure callback
     */

    public func requestGet<T: Codable>(request: APIRequest, returningClass: T.Type, success: @escaping (_ response: Any?) -> Void, failure: @escaping (APIError) -> Void) {
        self.request(url: request.url, method: .get, parameters: request.params, headers: request.httpHeaders, returningClass: returningClass, success: success, failure: failure)
    }

    public func requestPut<T: Codable>(request: APIRequest, returningClass: T.Type, success: @escaping (_ response: Any?) -> Void, failure: @escaping (APIError) -> Void) {
        self.request(url: request.url, method: .put, parameters: request.params, headers: request.httpHeaders, returningClass: returningClass, success: success, failure: failure)
    }

    public func requestDelete<T: Codable>(request: APIRequest, returningClass: T.Type, success: @escaping (_ response: Any?) -> Void, failure: @escaping (APIError) -> Void) {
        self.request(url: request.url, method: .delete, parameters: request.params, headers: request.httpHeaders, returningClass: returningClass, success: success, failure: failure)
    }

    public func requestPost<T: Codable>(request: APIRequest, returningClass: T.Type, success: @escaping (_ response: Any?) -> Void, failure: @escaping (APIError) -> Void) {
        self.request(url: request.url, method: .post, parameters: request.params, headers: request.httpHeaders, returningClass: returningClass, success: success, failure: failure)
    }

    private func request<T: Codable>(url: URLConvertible, method: HTTPMethod, parameters: Parameters?, headers: [String: String], returningClass: T.Type, success: @escaping (_ response: Any?) -> Void, failure: @escaping (APIError) -> Void) {
        if NetworkStatus.sharedInstance.reachabilityManager?.isReachable ?? false {
            var requestHeaders: HTTPHeaders = [:]
            for (key, value) in headers {
                let header = HTTPHeader(name: key, value: value)
                requestHeaders.add(header)
            }
            AF.request(url, method: method, parameters: parameters, encoding: JSONEncoding.default, headers: requestHeaders)
                .validate(statusCode: 200..<300)
                .responseJSON { response in
                    switch response.result {
                    case .success:
                        do {
                            if let dict = response.value as? [String: Any] {
                                let decoder = JSONDecoder()
                                let jsonData = try JSONSerialization.data(withJSONObject: dict, options: .prettyPrinted)
                                let res = self.getJsonStringFor(dictionary: jsonData)
                                let result = try decoder.decode(T.self, from: res.data(using: .utf8)!)
                                success(result)
                            }
                        } catch let err{
                            let error = err as NSError
                            failure(APIError(code: error.code, data: nil, error: error))
                        }
                    case .failure(let error):
                        if let data = response.data {
                            do {
                                _ = try JSONSerialization.jsonObject(with: data, options: [])
                            } catch {
                            }
                            if let response = response.response {
                                // Manage and customize internal error view based on error code 503
                                if response.statusCode == 503 {
                                    failure(APIError(code: response.statusCode, data: data, error: error as NSError))
                                    return
                                }
                            }

                            failure(APIError(code: response.response!.statusCode, data: data, error: error as NSError))
                        } else {
                            let err = error as NSError
                            failure(APIError(code: err.code , data: nil, error: error as NSError))
                        }
                    }
            }
        } else {
            let error = self.getNetworkError()
            failure(APIError(code: error.code, data: nil, error: error))
        }
    }

    public func multipartRequest<T: Codable>(request: MultipartAPIRequest, returningClass: T.Type, success: @escaping (_ response: Any?) -> Void, failure: @escaping (APIError?) -> Void) {
        do {
            let url = try URLRequest.init(url: request.url, method: .post, headers: HTTPHeaders(request.httpHeaders))
            AF.upload(multipartFormData: { (multiPart) in
                for (key, value) in request.params {
                    multiPart.append(value.data(using: .utf8)!, withName: key)
                }
                if let imgData = request.data {
                    multiPart.append(imgData, withName: request.dataKeyName, fileName: "\(request.imgName ?? "File.jpeg")", mimeType: "image/jpeg")
                }
            }, with: url)
                .uploadProgress(queue: .main, closure: { _ in
                })
                .validate(statusCode: 200..<300)
                .responseJSON(completionHandler: { response in
                    //Do what ever you want to do with response
                    switch response.result {
                    case .success:
                        do {
                            if let dict = response.value as? [String: Any] {
                                let decoder = JSONDecoder()
                                let jsonData = try JSONSerialization.data(withJSONObject: dict, options: .prettyPrinted)
                                let res = self.getJsonStringFor(dictionary: jsonData)
                                let result = try decoder.decode(T.self, from: res.data(using: .utf8)!)
                                success(result)
                            }
                        } catch let err {
                            let error = err as NSError
                            failure(APIError(code: error.code, data: nil, error: error))
                        }
                    case .failure(let error):
                        if let data = response.data {
                            do {
                                _ = try JSONSerialization.jsonObject(with: data, options: [])
                            } catch {
                            }
                            if let response = response.response {
                                // Manage and customize internal error view based on error code 503
                                if response.statusCode == 503 {
                                    failure(APIError(code: response.statusCode, data: data, error: error as NSError))
                                    return
                                }
                            }

                            failure(APIError(code: response.response!.statusCode, data: data, error: error as NSError))
                        } else {
                            let err = error as NSError
                            failure(APIError(code: err.code , data: nil, error: error as NSError))
                        }
                    }
                })
        } catch {
        }
    }
}
