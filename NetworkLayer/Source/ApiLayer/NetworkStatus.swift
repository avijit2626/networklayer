/**
 This class is used to Handle and check Internet reachability.
 Implemted using singleton pattern.
 */
import Foundation
import Alamofire

class NetworkStatus {

    // MARK: - Singleton instance
    static let sharedInstance = NetworkStatus()

    private init() {}

    // MARK: - Local property
    let reachabilityManager = NetworkReachabilityManager(host: "www.apple.com")

    // MARK: - Helper methods

    /**
     This method observe the network change
     */
    func startNetworkReachabilityObserver() {
        reachabilityManager?.startListening()
        reachabilityManager?.listener = { status in
            switch status {
            case .notReachable:
                break
            case .unknown :
                break
            case .reachable(.ethernetOrWiFi):
                break
            case .reachable(.wwan):
                break
            }
        }

    }
}
